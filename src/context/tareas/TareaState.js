import React, { useReducer } from 'react';
import TareaContext from "./TareaContext";
import TareaReducer from "./TareaReducer";

import {
   TAREAS_PROYECTO,
   AGREGAR_TAREA,
   VALIDAR_TAREA,
   ELIMINAR_TAREA,
   TAREA_ACTUAL,
   ACTUALIZAR_TAREA
} from "../../types";

import clienteAxios from "../../config/axios";

const TareaState = (props) => {

   const initialState = {
      tareasproyecto: [],
      errortarea: false,
      tareaseleccionada: null,
   };

   // crear dispatch y state
   const [state, dispatch] = useReducer(TareaReducer, initialState);

   // filtra tareas por proyecto
   const obtenerTareas = async (proyecto) => {
      try {
         const res = await clienteAxios.get("/api/tareas", { params: { proyecto } });
         dispatch({
            type: TAREAS_PROYECTO,
            payload: res.data.tareas
         });
      } catch (error) {
         console.log(error);
      }

   };

   // agregar una tarea al proyecto
   const agregarTarea = async (tarea) => {
      try {
         const res = await clienteAxios.post("/api/tareas", tarea);
         console.log(res);
         dispatch({
            type: AGREGAR_TAREA,
            payload: tarea
         });
      } catch (error) {
         console.log(error);

      }
   };

   // valida y muestra un error en caso de que sea necesario
   const validarTarea = () => {
      dispatch({
         type: VALIDAR_TAREA
      });
   };

   // elimina tarea
   const eliminarTarea = async (id, proyecto) => {
      try {
         await clienteAxios.delete(`/api/tareas/${id}`, {params: {proyecto}});
         dispatch({
            type: ELIMINAR_TAREA,
            payload: id
         })
      } catch (error) {
         console.log(error);
      }
   };

   // edita o modifica una tarea
   const actualizarTarea = async (tarea) => {
      try {
         const res = await clienteAxios.put(`/api/tareas/${tarea._id}`, tarea);

         dispatch({
            type: ACTUALIZAR_TAREA,
            payload: res.data.tarea
         });
      } catch (error) {
         console.log(error)
      }
   };

   // extrae una tarea para edicion
   const guardarTareaActual = (tarea) => {
      dispatch({
         type: TAREA_ACTUAL,
         payload: tarea
      });
   };



   return (
      <TareaContext.Provider
         value={ {
            tareasproyecto: state.tareasproyecto,
            errortarea: state.errortarea,
            tareaseleccionada: state.tareaseleccionada,
            obtenerTareas,
            agregarTarea,
            validarTarea,
            eliminarTarea,
            guardarTareaActual,
            actualizarTarea
         } }
      >
         { props.children }
      </TareaContext.Provider>
   );
};

export default TareaState;