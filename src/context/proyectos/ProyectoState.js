import React, { useReducer } from 'react';
import ProyectoContext from "./ProyectoContext";
import ProyectoReducer from "./ProyectoReducer";
import {
   FORMULARIO_PROYECTO,
   OBTENER_PROYECTOS,
   AGREGAR_PROYECTO,
   VALIDAR_FORMULARIO,
   PROYECTO_ACTUAL,
   ELIMINAR_PROYECTO,
   PROYECTO_ERROR
} from "../../types";

import clienteAxios from "../../config/axios";


const ProyectoState = (props) => {

   const initialState = {
      proyectos: [],
      formulario: false,
      errorform: false,
      proyecto: null,
      mensaje: null
   };

   // dispatch para ejecutar las acciones
   const [state, dispatch] = useReducer(ProyectoReducer, initialState);

   // funciones para el CRUD
   const mostrarFormulario = () => {
      dispatch({
         type: FORMULARIO_PROYECTO
      });
   };

   // obtener los proyectos 
   const obtenerProyectos = async () => {
      try {
         const res = await clienteAxios.get("/api/proyectos");

         dispatch({
            type: OBTENER_PROYECTOS,
            payload: res.data.proyectos
         });
      } catch (error) {
         const alerta = {
            msg: "Hubo un error",
            categoria: "alerta-error"
         }
         dispatch({
            type: PROYECTO_ERROR,
            payload: alerta
         })
      }
   };

   // agregar nuevo proyecto
   const agregarProyecto = async (proyecto) => {
      try {
         const res = await clienteAxios.post("/api/proyectos", proyecto);
         dispatch({
            type: AGREGAR_PROYECTO,
            payload: res.data
         });
      } catch (error) {
         const alerta = {
            msg: "Hubo un error",
            categoria: "alerta-error"
         }
         dispatch({
            type: PROYECTO_ERROR,
            payload: alerta
         })
      }
   };

   // validar formularios por errores
   const mostrarError = () => {
      dispatch({
         type: VALIDAR_FORMULARIO
      });
   };

   // selecciona el proyecto que el usuario dió click
   const proyectoActual = (proyectoId) => {
      dispatch({
         type: PROYECTO_ACTUAL,
         payload: proyectoId
      });
   };

   const eliminarProyecto = async (proyectoId) => {
      try {
         await clienteAxios.delete(`/api/proyectos/${ proyectoId }`);
         dispatch({
            type: ELIMINAR_PROYECTO,
            payload: proyectoId
         });
      } catch (error) {
         const alerta = {
            msg: "Hubo un error",
            categoria: "alerta-error"
         }
         dispatch({
            type: PROYECTO_ERROR,
            payload: alerta
         })
      }
   };

   return (
      <ProyectoContext.Provider
         value={ {
            proyectos: state.proyectos,
            formulario: state.formulario,
            errorform: state.errorform,
            proyecto: state.proyecto,
            mensaje: state.mensaje,
            mostrarFormulario,
            obtenerProyectos,
            agregarProyecto,
            mostrarError,
            proyectoActual,
            eliminarProyecto
         } }
      >
         { props.children }
      </ProyectoContext.Provider>
   );
};

export default ProyectoState;