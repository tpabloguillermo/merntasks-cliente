import React, { Fragment, useContext } from 'react';
import Tarea from "./Tarea";

import ProyectoContext from "../../context/proyectos/ProyectoContext";
import TareaContext from "../../context/tareas/TareaContext";
import {CSSTransition, TransitionGroup} from "react-transition-group";

const ListadoTarea = () => {

   // extraer proyectos del state inicial
   const proyectosContext = useContext(ProyectoContext);
   const {proyecto, eliminarProyecto} = proyectosContext;

   // context de tarea
   const tareasContext = useContext(TareaContext);
   const { tareasproyecto } = tareasContext;

   // si no hay proyecto seleccionado
   if(!proyecto) return <h2> Seleccion un proyecto </h2>;

   // extraemos el proyecto actual
   const [proyectoActual] = proyecto;

   return (
      <Fragment>
         <h2>Proyecto: {proyectoActual.nombre}</h2>
         <ul className="listado-tareas">
            { tareasproyecto.length === 0 ?
               <li className="tarea"> <p> No hay tareas</p> </li>
            :
               <TransitionGroup>
                  {tareasproyecto.map(tarea => (
                     <CSSTransition
                        key={tarea._id}
                        timeout={200}
                        classNames="tarea"
                     >
                        <Tarea tarea={ tarea }/>
                     </CSSTransition>
                  ))}
               </TransitionGroup> 
            }
         </ul>

         <button
            type="button"
            className="btn btn-eliminar"
            onClick={() => eliminarProyecto(proyectoActual._id)}
         >
            Eliminar Proyecto &times;
         </button>
      </Fragment>
   );
};

export default ListadoTarea;