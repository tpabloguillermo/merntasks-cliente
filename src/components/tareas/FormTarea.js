import React, { useContext, useState, useEffect } from 'react';
import ProyectoContext from "../../context/proyectos/ProyectoContext";
import TareaContext from "../../context/tareas/TareaContext";

const FormTarea = () => {

   const proyectosContext = useContext(ProyectoContext);
   const { proyecto } = proyectosContext;

   // context de tarea
   const tareasContext = useContext(TareaContext);
   const { errortarea, tareaseleccionada, agregarTarea, validarTarea, obtenerTareas,
      actualizarTarea } = tareasContext;

   // state form
   const [tarea, guardarTarea] = useState({
      nombre: ""
   });

   // useEffect q detecta una tarea seleccionada
   useEffect(() => {
      if (tareaseleccionada !== null) {
         // o sea cuando tengo una tarea seleccionada guardo la tarea en el state local
         // y esto va a hacer que aparezca el nombre de la tarea en el campo de texto pa modificar
         guardarTarea(tareaseleccionada);
      } else {
         guardarTarea({
            nombre: ""
         });
      }
   }, [tareaseleccionada]);

   // extraer nombre de la tarea
   const { nombre } = tarea;

   if (!proyecto) return null;

   const [proyectoActual] = proyecto;

   // leer valores del form
   const handleChange = (e) => {
      guardarTarea({
         ...tarea,
         [e.target.name]: e.target.value
      });
   };

   const onSubmit = (e) => {
      e.preventDefault();

      // validar
      if (nombre.trim() === "") {
         validarTarea();
         return;
      }

      // se revisa si es edicion o está agregando una nueva tarea
      if (tareaseleccionada === null) {
         // agregar nueva tarea al state
         tarea.proyecto = proyectoActual._id;
         agregarTarea(tarea);
      } else {
         // actualizar tarea existente
         actualizarTarea(tarea);
      }

      // actualizar tareas 
      obtenerTareas(proyectoActual.id);

      // reiniciar el form
      guardarTarea({
         nombre: ""
      });


   };

   return (
      <div className="formulario">
         <form
            onSubmit={ onSubmit }
         >
            <div className="contenedor-input">
               <input
                  type="text"
                  className="input-text"
                  placeholder="Nombre Tarea"
                  name="nombre"
                  value={ nombre }
                  onChange={ handleChange }
               />
            </div>
            <div className="contenedor-input">
               <input
                  type="submit"
                  className="btn btn-primario btn-submit btn-block"
                  value={ tareaseleccionada ? "Editar tarea" : "Agregar tarea" }
               />
            </div>
         </form>
         {errortarea ? <p className="mensaje error"> El nombre es obligatorio</p> : null }
      </div>
   );
};

export default FormTarea;