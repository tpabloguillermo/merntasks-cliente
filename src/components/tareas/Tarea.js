import React, {useContext} from 'react';
import ProyectoContext from "../../context/proyectos/ProyectoContext";
import TareaContext from "../../context/tareas/TareaContext";

const Tarea = ({ tarea }) => {

   // context de proyecto
   const proyectosContext = useContext(ProyectoContext);
   const { proyecto } = proyectosContext;

   // extraer el proyecto
   const [proyectoAct] = proyecto;

   // context de tarea
   const tareasContext = useContext(TareaContext);
   const { eliminarTarea, obtenerTareas, guardarTareaActual, actualizarTarea } = tareasContext;

   const handleDelete = (id) => {
      eliminarTarea(id, proyectoAct._id);
      obtenerTareas(proyectoAct._id);
   }

   const handleChange = (tarea) => {
      tarea.estado = !(tarea.estado);
      actualizarTarea(tarea);
      
   }

   // agrega una tarea actual cuando el usuario quiere editarla
   const seleccionarTarea = (tarea) => {
      guardarTareaActual(tarea);
   }

   return (
      <li className="tarea sombra">
         <p> { tarea.nombre } </p>
         <div className="estado">
            { tarea.estado
               ?
               (
                  <button
                     type="button"
                     className="completo"
                     onClick={() => handleChange(tarea)}
                  >
                     Completo
                  </button>
               )
               :
               (
                  <button
                     type="button"
                     className="incompleto"
                     onClick={() => handleChange(tarea)}
                  >
                     Incompleto
                  </button>
               )
            }
         </div>
         <div className="acciones">
            <button
               type="button"
               className="btn btn-primario"
               onClick={() => seleccionarTarea(tarea)}
            >
               Editar
            </button>
            <button
               type="button"
               className="btn btn-secundario"
               onClick={() => handleDelete(tarea._id)}
            >
               Eliminar
            </button>
         </div>
      </li>
   );
};

export default Tarea;