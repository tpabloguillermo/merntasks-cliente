import React, { useState, useContext, useEffect } from 'react';
import {Link} from "react-router-dom"
import AlertaContext from "../../context/alertas/alertaContext";
import AuthContext from "../../context/autenticacion/authContext";

const NuevaCuenta = (props) => {

   // valores del context
   const alertaContext = useContext(AlertaContext);
   const {alerta, mostrarAlerta} = alertaContext;

   const authContext = useContext(AuthContext);
   const {mensaje, autenticado, registrarUsuario} = authContext;

   // en caso de que el registro sea correcto o sea un registro duplicado
   useEffect(() => {
      if(autenticado) {
         props.history.push("/proyectos");
      }
      if(mensaje) {
         mostrarAlerta(mensaje.msg, mensaje.categoria)
      }
      // eslint-disable-next-line
   }, [mensaje,autenticado, props.history]);

   // state para el registro
   const [usuario, guardarUsuario] = useState({
      nombre: "",
      email: "",
      password: "",
      confirmar: "",
   });

   // extraer del usuario
   const { nombre, email, password, confirmar } = usuario;

   const onChange = (e) => {
      guardarUsuario({
         ...usuario,
         [e.target.name]: e.target.value
      });
   };

   // cuando se registra
   const onSubmit = (e) => {
      e.preventDefault();

      // validar los campos
      if (nombre.trim() === "" || email.trim() === "" || password.trim() === "" || confirmar.trim()==="") {
         mostrarAlerta("Todos los campos son obligatorios", "alerta-error");
         return;
      } 

      // pw minimo de 6 chars
      if(password.length < 6) {
         mostrarAlerta("El password debe ser de al menos 6 caracteres", "alerta-error");
         return;
      }

      // pw iguales
      if(password !== confirmar) {
         mostrarAlerta("Los passwords tienen que ser iguales", "alerta-error");
         return;
      }
      
      // pasarlo al action
      registrarUsuario({
         nombre, email, password
      })
   };

   return (
      <div className="form-usuario">
         {alerta? 
            ( 
               <div className={`alerta ${alerta.categoria}`}> {alerta.msg} </div>
            ) 
         : 
            null
         }
         <div className="contenedor-form sombra-dark">
            <h1> Obtener una cuenta </h1>
            <form
               onSubmit={ onSubmit }
            >
               <div className="campo-form">
                  <label htmlFor="email"> Nombre </label>
                  <input
                     type="text"
                     id="nombre"
                     name="nombre"
                     placeholder="Tu nombre"
                     value={ nombre }
                     onChange={ onChange }
                  />
               </div>
               <div className="campo-form">
                  <label htmlFor="email"> Email </label>
                  <input
                     type="email"
                     id="email"
                     name="email"
                     placeholder="Tu email"
                     value={ email }
                     onChange={ onChange }
                  />
               </div>
               <div className="campo-form">
                  <label htmlFor="password"> Password </label>
                  <input
                     type="password"
                     id="password"
                     name="password"
                     placeholder="Tu password"
                     value={ password }
                     onChange={ onChange }
                  />
               </div>
               <div className="campo-form">
                  <label htmlFor="password"> Confirmar password </label>
                  <input
                     type="password"
                     id="confirmar"
                     name="confirmar"
                     placeholder="Repite tu password"
                     value={ confirmar }
                     onChange={ onChange }
                  />
               </div>

               <div className="campo-form">
                  <input
                     type="submit"
                     className="btn btn-primario btn-block"
                     value="Registrarme"
                  />
               </div>
            </form>

            <Link to="/" className="enlace-cuenta"> Iniciar Sesión </Link>
         </div>
      </div>
   );
};

export default NuevaCuenta;