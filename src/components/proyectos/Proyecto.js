import React, { useContext } from 'react';
import ProyectoContext from "../../context/proyectos/ProyectoContext";
import TareaContext from "../../context/tareas/TareaContext";

const Proyecto = ({ proyecto }) => {
   // context de proyecto
   const proyectosContext = useContext(ProyectoContext);
   const { proyectoActual } = proyectosContext;

   // context de tarea
   const tareasContext = useContext(TareaContext);
   const { obtenerTareas } = tareasContext;

   const { nombre, _id } = proyecto;

   const onClick = (id) => {
      proyectoActual(id); // fijar proyecto actual
      obtenerTareas(id); // filtrar tareas del proyecto
   };

   return (
      <li>
         <button
            type="button"
            className="btn btn-blank"
            onClick={ () => onClick(_id)  }
         >
            { nombre }
         </button>
      </li>
   );
};

export default Proyecto;