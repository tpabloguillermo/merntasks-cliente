import React, { Fragment, useState, useContext } from 'react';
import ProyectoContext from "../../context/proyectos/ProyectoContext";

const NuevoProyecto = () => {

   // obtener el state del formulario
   const proyectosContext = useContext(ProyectoContext);
   const { formulario, errorform, mostrarFormulario, agregarProyecto, mostrarError } = proyectosContext;

   // state de nuevo proyecto
   const [proyecto, guardarProyecto] = useState({
      nombre: ""
   });

   // extraer nombre proyecto
   const { nombre } = proyecto;

   // lee contenidos del input
   const onChange = (e) => {
      guardarProyecto({
         ...proyecto,
         [e.target.name]: e.target.value
      });
   };

   // cuando se envia un proyecto
   const onSubmit = (e) => {
      e.preventDefault();

      // validar
      if (nombre === "") {
         mostrarError();
         return;
      }

      // agregar al state
      agregarProyecto(proyecto);

      // reiniciar form
      guardarProyecto({
         nombre: ""
      });
   };

   return (
      <Fragment>
         <button
            type="button"
            className="btn btn-block btn-primario"
            onClick={ () => mostrarFormulario() }
         >
            Nuevo Proyecto
         </button>

         {formulario ?
            (
               <form
                  className="formulario-nuevo-proyecto"
                  onSubmit={ onSubmit }
               >
                  <input
                     type="text"
                     className="input-text"
                     placeholder="Nombre Proyecto"
                     name="nombre"
                     value={ nombre }
                     onChange={ onChange }
                  />

                  <input
                     type="submit"
                     className="btn btn-primario btn-block"
                     value="Agregar Proyecto"
                  />
               </form>

            )
         : null }
         {errorform ? <p className="mensaje error"> El nombre es obligatorio </p> : null }
      </Fragment>
   );
};

export default NuevoProyecto;